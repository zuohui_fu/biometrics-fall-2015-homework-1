__author__ = 'fuzuohui'
import numpy as np
import cv2
from matplotlib import pyplot as plt

number_folder=1
while (number_folder)<11:
    face_cascade =cv2.CascadeClassifier('/usr/local/Cellar/Opencv/2.4.12/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml')
    #eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
    file_r=open("/Users/fuzuohui/Desktop/PHD/15Fall/Biometrics/project1/hw1/biometrics-fall-2015-homework-1/data/FDDB-folds/FDDB-fold-"+"0"+str(number_folder)+".txt",'r')
    file_w=open("/Users/fuzuohui/Desktop/PHD/15Fall/Biometrics/project1/hw1/biometrics-fall-2015-homework-1/data/fold-"+"0"+str(number_folder)+"-out.txt",'w')
    img_base='/Users/fuzuohui/Desktop/PHD/15Fall/Biometrics/project1/hw1/biometrics-fall-2015-homework-1/data/FDDB-folds/'
    img_format='.ppm'
    for line in file_r.readlines():
        img_path=img_base+line[:-1]+img_format
        img_name=line

        print img_path

        ################################detection part
        img= cv2.imread(img_path)
        img=np.array(img)
        #cv2.imshow("2",img_path)
        #cv2.waitKey(10000)
        #img=cv2.resize(img_ori,None,fx=0.4, fy=0.4, interpolation = cv2.INTER_CUBIC)##
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_detect = face_cascade.detectMultiScale(gray,1.8,0) #Detects objects of different sizes in the input image. The detected objects are returned as a list of rectangles.
        rectList,weights=cv2.groupRectangles(np.array(face_detect).tolist(),5)#Groups the object candidate rectangles.
        if len(rectList)>0:
            result=list()
            rectList_new = rectList.tolist()
            file_w.write(img_name+str(len(rectList))+'\n')

            i=0
            for (x,y,w,h) in rectList:


                cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]

                score=str(int(weights[i]))
                cv2.putText(img,score,(x,y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,0))
                i=i+1
                file_w.write(str(x)+' '+str(y)+' '+str(w)+' '+str(h)+' '+score+'\n')

            #cv2.imshow('img',img)
            #cv2.waitKey(0)
            print weights


        else:


            file_w.write(img_name+"0"+'\n')
    number_folder=number_folder+1
